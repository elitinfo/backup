#!/bin/bash

# dump every databases
for db in `mysql --batch -N -e 'show databases' | grep -v -E '_schema|mysql|sys'`
do
    if [ -t 1 ]; then
	echo Dumping SQL database $db...
    fi
    mysqldump --single-transaction=TRUE --hex-blob --routines --events --triggers --default-character-set=utf8 $db > /var/www/virtual/$db.sql
done

# backup all important folders including dumps created before
RESTIC_CMD="restic backup --exclude-caches --tag daily /etc /home /root /var/www/virtual /mnt/adat"

# Check if the script is run from a terminal
if [ -t 1 ]; then
    # Run restic without the -q option
    $RESTIC_CMD
else
    # Run restic with the -q option for quiet mode
    $RESTIC_CMD -q
fi

# delete temporary sql dumps
rm /var/www/virtual/*.sql
